from django.urls import path
from tasks.views import create_task, assign_task


urlpatterns = [
    path("mine/", assign_task, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
