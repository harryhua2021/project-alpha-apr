from django.urls import path
from projects.views import project_list, project_view, create_project


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("projects/<int:id>/", project_view, name="show_project"),
]
