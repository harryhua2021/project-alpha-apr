# Generated by Django 4.2.2 on 2023-06-05 21:45

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="description",
            field=models.TextField(max_length=20),
        ),
    ]
